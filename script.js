document.addEventListener('DOMContentLoaded', function() {
    const addTaskBtn = document.getElementById('addTaskBtn');
    const taskList = document.getElementById('taskList');

    addTaskBtn.addEventListener('click', function() {
        const taskTitle = prompt('Enter task title:');
        if (taskTitle) {
            const li = document.createElement('li');
            li.textContent = taskTitle;
            taskList.appendChild(li);
        }
    });
});

